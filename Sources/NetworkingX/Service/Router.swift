//
//  Router.swift
//  Fishing Nets
//
//  Created by Timothy Barnard on 03/08/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//

import Foundation

public enum Result: Equatable {
    case success(Int)
    case failure(Int)
}

public func ==(lhs: Result, rhs: Result) -> Bool {
    if case .success(let lCode) = lhs, case .success(let rCode) = rhs {
        return lCode == rCode
    }
    if case .failure(let lCode) = lhs, case .failure(let rCode) = rhs {
        return lCode == rCode
    }
    return false
}

public typealias NetworkRouterCompletion = (_ data: Data?, _ error: Error?, _ result: Result?) -> Void


public protocol NetworkRouter: class {
    func request(_ route: EndPointType, completion: @escaping NetworkRouterCompletion)
    func cancel()
}

public class Router: NetworkRouter {
    private var task: URLSessionTask?
    private var session: URLSession?

    public init(_ session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    public func request(_ route: EndPointType, completion: @escaping NetworkRouterCompletion) {
        do {
            var request = try self.buildRequest(from: route)
            request.allHTTPHeaderFields = [
                "Content-Type": "application/json",
            ]
            NetworkLogger.log(request: request)
            task = session?.dataTask(with: request, completionHandler: { data, response, error in
                NetworkLogger.log(response: response)
                DispatchQueue.main.async {
                    if let response = response as? HTTPURLResponse {
                        let result = self.handleNetworkResponse(response, data: data)
                        completion(data, error, result)
                    } else {
                        print(error.debugDescription)
                        completion(data, error, .failure(-1))
                    }
                }
            })
        } catch {
            completion(nil, error, .failure(-1))
        }

        self.task?.resume()
    }

    public func cancel() {
        self.task?.cancel()
    }

    fileprivate func buildRequest(from route: EndPointType) throws -> URLRequest {

        var request = URLRequest(url: route.baseURL.appendingPathComponent(route.path),
                                 cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                 timeoutInterval: 30.0)

        request.httpMethod = route.httpMethod.rawValue
        if let headers = route.headers {
            request.allHTTPHeaderFields = headers
        }
        do {
            switch route.task {
            case .request:
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            case .requestParameters(let bodyParameters,
                                    let bodyEncoding,
                                    let urlParameters):

                try self.configureParameters(bodyParameters: bodyParameters,
                                             bodyEncoding: bodyEncoding,
                                             urlParameters: urlParameters,
                                             request: &request)

            case .requestParametersAndHeaders(let bodyParameters,
                                              let bodyEncoding,
                                              let urlParameters,
                                              let additionalHeaders):

                self.addAdditionalHeaders(additionalHeaders, request: &request)
                try self.configureParameters(bodyParameters: bodyParameters,
                                             bodyEncoding: bodyEncoding,
                                             urlParameters: urlParameters,
                                             request: &request)
            }
            return request
        } catch {
            throw error
        }
    }

    fileprivate func configureParameters(bodyParameters: Parameters?,
                                         bodyEncoding: ParameterEncoding,
                                         urlParameters: Parameters?,
                                         request: inout URLRequest) throws {
        do {
            try bodyEncoding.encode(urlRequest: &request,
                                    bodyParameters: bodyParameters, urlParameters: urlParameters)
        } catch {
            throw error
        }
    }

    fileprivate func addAdditionalHeaders(_ additionalHeaders: HTTPHeaders?, request: inout URLRequest) {
        guard let headers = additionalHeaders else { return }
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
    }
    
    fileprivate func handleNetworkResponse(_ response: HTTPURLResponse, data: Data?) -> Result {
        switch response.statusCode {
        case 200...299: return .success(response.statusCode)
        case 401...500: return .failure(response.statusCode)
        case 501...599: return .failure(response.statusCode)
        case 600: return .failure(response.statusCode)
        default: return .failure(response.statusCode)
        }
    }

}
