//
//  HTTPMethod.swift
//  Fishing Nets
//
//  Created by Timothy Barnard on 03/08/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//

import Foundation

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}
