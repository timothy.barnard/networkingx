//
//  NetworkingManager.swift
//  Redmine
//
//  Created by Timothy on 08/03/2019.
//  Copyright © 2019 Barnard Developments. All rights reserved.
//

import Foundation

public struct NetworkManager {
    let router = Router()
    public init() {}
    public func request(endPoint: EndPointType, completion: @escaping (_ data: Data?,_ error: Error?,_ result: Result?) -> Void ) {
        router.request(endPoint) { (data, error, result) in
            DispatchQueue.main.async {
               completion(data, error, result)
            }
        }
    }
}
