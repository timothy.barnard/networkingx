import XCTest

import NetworkingXTests

var tests = [XCTestCaseEntry]()
tests += NetworkingXTests.allTests()
XCTMain(tests)
