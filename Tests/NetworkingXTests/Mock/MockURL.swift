//
//  File.swift
//  
//
//  Created by Timothy on 05/06/2019.
//

@testable import NetworkingX
import Foundation

enum TestAPI {
    case loginTest
}

extension TestAPI: EndPointType {
    var baseURL: URL {
        return URL(string: "https://timothybarnard.org")!
    }
    var path: String {
        switch self {
        case .loginTest: return "/login"
        }
    }
    var httpMethod: HTTPMethod {
        switch self {
        case .loginTest: return .post
        }
    }
    var task: HTTPTask {
        return .request
    }
    var headers: HTTPHeaders? { return nil }
}

class URLSessionMock: URLSession {
    // Properties that enable us to set exactly what data or error
    // we want our mocked URLSession to return for any request.
    var data: Data?
    var error: Error?
    var urlResponse: URLResponse?
    
    override func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return URLSessionDataTaskMock {
            completionHandler(self.data, self.urlResponse, self.error)
        }
    }
}

// We create a partial mock by subclassing the original class
class URLSessionDataTaskMock: URLSessionDataTask {
    private let closure: () -> Void
    
    init(closure: @escaping () -> Void) {
        self.closure = closure
    }
    
    // We override the 'resume' method and simply call our closure
    // instead of actually resuming any task.
    override func resume() {
        closure()
    }
}
