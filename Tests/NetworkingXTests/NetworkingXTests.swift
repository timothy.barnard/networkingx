import XCTest
@testable import NetworkingX

final class NetworkingXTests: XCTestCase {
    
    var router: Router!
    
    func testRequest() {
        let login = TestAPI.loginTest
        let session = URLSessionMock()
        session.data = Data([0, 1, 0, 1])
        session.urlResponse = HTTPURLResponse(url: login.baseURL, statusCode: 200, httpVersion: "HTTP/2", headerFields: nil)
        
        router = Router(session)
        let expectation = self.expectation(description: "Login Request")
        var resultData: Data?
        var resultError: Error?
        var resultResult: Result?
        router.request(login) { (data, error, result) in
            resultData = data
            resultError = error
            resultResult = result
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(resultData)
        XCTAssertNil(resultError)
        XCTAssertEqual(resultResult, Result.success(200))
    }

    static var allTests = [
        ("testRequest", testRequest),
    ]
}
